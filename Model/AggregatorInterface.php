<?php

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Model;

interface AggregatorInterface
{
    /**
     * @return int|string|array|null
     */
    public function aggregate();
}