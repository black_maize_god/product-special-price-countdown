<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Model;

abstract class AbstractAggregator implements \PerspectiveStudio\ProductSpecialPriceCountdown\Model\AggregatorInterface
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }
}