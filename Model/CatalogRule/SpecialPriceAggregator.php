<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Model\CatalogRule;

use PerspectiveStudio\ProductSpecialPriceCountdown\Model\AbstractSpecialPriceAggregator as AbstractSpecPriceAggregator;

class SpecialPriceAggregator extends AbstractSpecPriceAggregator
{
    /**
     * @var \Magento\CatalogRule\Model\ResourceModel\Rule $ruleResource
     */
    private $ruleResource;

    /**
     * @var \Magento\Customer\Model\Session $customerSession
     */
    private $customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    private $storeManager;

    /**
     * @var \Magento\CatalogRule\Api\CatalogRuleRepositoryInterface $ruleRepository
     */
    private $ruleRepository;

    /**
     * @param \Magento\CatalogRule\Model\ResourceModel\Rule $ruleResource
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Catalog\Model\Session $catalogSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\CatalogRule\Api\CatalogRuleRepositoryInterface $ruleRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\CatalogRule\Model\ResourceModel\Rule $ruleResource,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\CatalogRule\Api\CatalogRuleRepositoryInterface $ruleRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->ruleResource = $ruleResource;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManager;
        $this->ruleRepository = $ruleRepository;

        parent::__construct(
            $logger,
            $catalogSession,
            $productRepository
        );
    }

    /**
     * @inheritDoc
     */
    public function aggregate(): array
    {
        $catalogPerCurrentProductRules = [];

        try {
            $currentProduct = $this->getProduct();

            if (!$currentProduct || !$currentProduct->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    new \Magento\Framework\Phrase('Product is Incorrect')
                );
            }

            /** @var array $catalogPerCurrentProductUnparsedRules */
            $catalogPerCurrentProductUnparsedRules = $this->ruleResource->getRulesFromProduct(
                date('Y-m-d H:i:s'),
                $this->storeManager->getStore()->getWebsiteId(),
                $this->customerSession->getCustomerGroupId(),
                (int) $currentProduct->getId()
            );

            foreach ($catalogPerCurrentProductUnparsedRules as $catalogPerCurrentProductUnparsedRule) {
                /** @var \Magento\SalesRule\Api\Data\RuleInterface $rule */
                $rule = $this->ruleRepository->get((int)$catalogPerCurrentProductUnparsedRule['rule_id']);
                $expires = date('Y-m-d H:i:s', (int)$catalogPerCurrentProductUnparsedRule['to_time']);

                if (strtotime($expires) < strtotime('now')) {
                    continue;
                }

                $catalogPerCurrentProductRule = [
                    'label' => $rule->getName(),
                    'expires' => $expires,
                    'product_id' => $currentProduct->getId()
                ];

                $catalogPerCurrentProductRules[] = $catalogPerCurrentProductRule;
            }
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $this->logger->error($exception->getMessage());
        }

        $this->clear();

        return $catalogPerCurrentProductRules;
    }
}
