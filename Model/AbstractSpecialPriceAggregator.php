<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Model;

/**
 * @method array aggregate()
 */
abstract class AbstractSpecialPriceAggregator extends AbstractAggregator
{
    /**
     * @var \Magento\Catalog\Model\Session $catalogSession
     */
    protected $catalogSession;

    /**
     * @var int|\Magento\Catalog\Api\Data\ProductInterface $product
     */
    protected $product = null;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    protected $productRepository;

    /**
     * AbstractSpecialPriceAggregator constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Model\Session $catalogSession
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Model\Session $catalogSession,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->catalogSession = $catalogSession;
        $this->productRepository = $productRepository;

        parent::__construct($logger);
    }

    /**
     * @param int|\Magento\Catalog\Api\Data\ProductInterface $product
     * @return $this
     */
    public function setProduct($product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return void
     */
    protected function clear(): void
    {
        $this->product = null;
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface|null
     */
    protected function getProduct(): ?\Magento\Catalog\Api\Data\ProductInterface
    {
        $currentProductId = $this->product ?? (int) $this->catalogSession->getData('last_viewed_product_id');

        if ($this->product instanceof \Magento\Catalog\Api\Data\ProductInterface) {
            return $this->product;
        }

        $product = null;

        try {
            $product = $this->productRepository->getById($currentProductId);
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            $this->logger->error($exception->getMessage() . " Requested Product ID = $currentProductId");
        }

        return $product;
    }
}
