<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Model\Product;

use PerspectiveStudio\ProductSpecialPriceCountdown\Model\AbstractSpecialPriceAggregator as AbstractSpecPriceAggregator;

class SpecialPriceAggregator extends AbstractSpecPriceAggregator
{
    /**
     * @inheritDoc
     */
    public function aggregate(): array
    {
        $currentProduct = $this->getProduct();
        $specialPrice = [];

        if (!$this->isCircumstancesFavorable($currentProduct)) {
            return $specialPrice;
        }

        $specialPrice = [
            'label' => __('Product Special Price: %1', $currentProduct->getName()),
            'expires' => $currentProduct->getSpecialToDate(),
            'product_id' => $currentProduct->getId()
        ];

        $this->clear();

        return $specialPrice;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface|null $product
     * @return bool
     */
    protected function isCircumstancesFavorable(\Magento\Catalog\Api\Data\ProductInterface $product): bool
    {
        return $product
            && $product->getId()
            && ($specialPriceEndDate = $product->getSpecialToDate())
            && strtotime($specialPriceEndDate) > strtotime('now');
    }
}
