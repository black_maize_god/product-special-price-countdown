<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Model\Product\Bundle;

class ProductPerSelectionAggregator extends \PerspectiveStudio\ProductSpecialPriceCountdown\Model\AbstractAggregator
{
    /**
     * @var int|null
     */
    protected $selectionId = null;

    /**
     * @var \Magento\Bundle\Model\ResourceModel\Selection\CollectionFactory $bundleSelectionCollectionFactory
     */
    private $bundleSelectionCollectionFactory;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Bundle\Model\ResourceModel\Selection\CollectionFactory $bundleSelectionCollectionFactory
    ) {
        $this->bundleSelectionCollectionFactory = $bundleSelectionCollectionFactory;

        parent::__construct($logger);
    }

    /**
     * This method returns productId by selectionId
     * @inheritDoc
     */
    public function aggregate(): int
    {
        if (!$this->selectionId) {
            $this->logger->error('Selection id is incorrect or does not set. Selection id = ' . $this->selectionId);

            return -1;
        }

        $bundleSelectionCollection = $this->bundleSelectionCollectionFactory->create();

        /** @var \Magento\Bundle\Model\Selection $bundleSelectionModel */
        $bundleSelectionModel = $bundleSelectionCollection
            ->addFilter('selection_id', (string) $this->selectionId, 'eq')
            ->getFirstItem();

        $this->clear();

        return (int) $bundleSelectionModel->getProductId();
    }

    /**
     * @param int $selectionId
     * @return $this
     */
    public function setSelectionId(int $selectionId): self
    {
        $this->selectionId = $selectionId;

        return $this;
    }

    /**
     * @return void
     */
    protected function clear(): void
    {
        $this->selectionId = null;
    }
}
