var config = {
    map: {
        '*': {
            jqueryCountDown: 'PerspectiveStudio_ProductSpecialPriceCountdown/js/lib/jquery.countdown.min'
        }
    },
    shim: {
        'jqueryCountDown' : ['jquery']
    }
};