define([
    'underscore',
    'jquery',
    'jqueryCountDown',
    'mage/translate'
], function (
    _,
    $
) {
    'use strict';

    return {
        /**
         * Countdown template context
         * @param {string} label
         * @return {string}
         */
        getCountdownTemplate: function (label) {
            return '<div class="countdown-item-label">' + label + '</div>' +
                '<div class="countdown-item-inner-wrapper">' +
                    '<div class="countdown-item-inner-content-wrapper">' +
                        '<div class="countdown-item-inner-content">' +
                            '<span class="inner-content-timer">%D</span>' +
                            '<span class="inner-content-label">' + $.mage.__('Days') + '</span>' +
                        '</div>' +
                    '</div>' +
                    '<div class="countdown-item-inner-content-wrapper">' +
                        '<div class="countdown-item-inner-content">' +
                            '<span class="inner-content-timer">%H</span>' +
                            '<span class="inner-content-label">' + $.mage.__('Hours') + '</span>' +
                        '</div>' +
                    '</div>' +
                    '<div class="countdown-item-inner-content-wrapper">' +
                        '<div class="countdown-item-inner-content">' +
                            '<span class="inner-content-timer">%M</span>' +
                            '<span class="inner-content-label">' + $.mage.__('Minutes') + '</span>' +
                        '</div>' +
                    '</div>' +
                    '<div class="countdown-item-inner-content-wrapper">' +
                        '<div class="countdown-item-inner-content">' +
                            '<span class="inner-content-timer">%S</span>' +
                            '<span class="inner-content-label">' + $.mage.__('Seconds') + '</span>' +
                        '</div>' +
                    '</div>' +
                '</div>';
        },

        /**
         * Connect countdown widget to the element
         * @param {Object} element
         * @param {Object} specialPrice
         */
        render: function (element, specialPrice) {
            if (!specialPrice
                || _.isEmpty(specialPrice)
            ) {
                return;
            }

            $(element)
                .countdown(specialPrice.expires, function (event) {
                    $(element).html(event.strftime(this.getCountdownTemplate(specialPrice.label)));
                }.bind(this))
                .on('finish.countdown', function () {
                    $(this).css('display', 'none').countdown('remove');
                });
        }
    };
});