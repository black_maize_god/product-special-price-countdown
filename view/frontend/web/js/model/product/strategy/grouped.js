define([
    'underscore',
    'jquery',
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/action/product-special-price'
], function (
    _,
    $,
    knockKnockToSpecialPricesApi
) {
    'use strict';

    return {
        /**
         * Callback for grouped products
         */
        handle: function (event) {
            var $element = $(event.currentTarget),
                eventDataSelector = $element.data('selector'),
                productId = eventDataSelector.substring(
                    eventDataSelector.lastIndexOf("[") + 1,
                    eventDataSelector.lastIndexOf("]")
                ),
                $groupedProductsInputsElements = $('[data-selector*="super_group"]'),
                canProcess = true,
                callback = function (response) {
                    if (response.length && !_.isEmpty(response[0])) {
                        this.specialPricesPool.push(response[0]);
                    }
                }.bind(this);

            $groupedProductsInputsElements.attr('disabled', 'disabled');

            if (Number($element.val()) < 1) {
                _.forEach(this.specialPricesPool(), function (item) {
                    if (item && item.product_id === productId) {
                        this.specialPricesPool.remove(item);
                    }
                }, this);

                $groupedProductsInputsElements.removeAttr('disabled');

                return;
            }

            _.forEach(this.specialPricesPool(), function (item) {
                if (item && item.product_id === productId) {
                    canProcess = false;

                    return true;
                }
            }, this);

            if (canProcess) {
                knockKnockToSpecialPricesApi.getSpecialPrice(productId, callback, this);
            }

            $groupedProductsInputsElements.removeAttr('disabled');
        }
    };
});