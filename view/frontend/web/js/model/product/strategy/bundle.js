define([
    'underscore',
    'jquery',
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/action/product-special-price-by-bundle-selection'
], function (
    _,
    $,
    knockKnockToSpecialPricesApi
) {
    'use strict';

    /**
     * Add special price to the special prices pool
     * @param {array} response
     */
    var addSpecialPriceCallback = function (response) {
        if (response.length && !_.isEmpty(response[0])) {
            this.specialPricesPool.push(response[0]);

            if (response[1] && response[1] > 0) {
                $('[name*="bundle_option"][value="' + response[1] + '"]')
                .data('product', response[0]['product_id']);
            }
        }
    };

    return {
        /**
         * Process all currently selected bundle products
         */
        handleAll: function () {
            var $bundleOptions = $('[name*="bundle_option"]:checked, [name*="bundle_option"]:checked:hidden');

            if ($bundleOptions.length && !this.specialPricesPool().length) {
                _.forEach($bundleOptions, function ($item) {
                    var itemName = $($item).attr('name'),
                        optionId = itemName.substring(
                            itemName.lastIndexOf("[") + 1,
                            itemName.lastIndexOf("]")
                        ),
                        $qtyInput = $('[data-selector*="bundle_option_qty[' + optionId + ']"]');

                    if (Number($qtyInput.val()) > 0) {
                        $qtyInput.data('prevValue', $qtyInput.val());

                        knockKnockToSpecialPricesApi.getSpecialPrice($item.value, addSpecialPriceCallback, this);
                    }
                }, this);
            }
        },

        /**
         * Callback for bundle products
         */
        handle: function (event) {
            var productIdsToRemove = [],
                selectionId = event.currentTarget.name.substring(
                    event.currentTarget.name.lastIndexOf("[") + 1,
                    event.currentTarget.name.lastIndexOf("]")
                );

            /*
             * Check if event triggered NOT BY HUMAN. In this case should make return,
             * because during this functional working with bundle product -
             * qty input changing trigger another event programmatically,
             * which listens this functional.
             * Check if changing not qty input.
             */
            if (event.originalEvent === undefined || event.currentTarget.name.indexOf('_qty') !== -1) {
                return;
            }

            _.forEach($('[data-selector*="' + event.currentTarget.name + '"]:not(:checked)'), function ($item) {
                var productId = $($item).data('product');

                if (productId) {
                    productIdsToRemove.push($($item).data('product'));
                }
            }, this);

            _.forEach(this.specialPricesPool(), function (item) {
                if (item && productIdsToRemove.indexOf(item.product_id) !== -1) {
                    this.specialPricesPool.remove(item);
                }
            }, this);

            if (Number($('[data-selector*="bundle_option_qty[' + selectionId + ']"]').val()) > 0) {
                knockKnockToSpecialPricesApi.getSpecialPrice(event.currentTarget.value, addSpecialPriceCallback, this);
            }
        },

        /**
         * Callback for changing options qty for bundle product
         */
        handleQtyChanging: function (event) {
            var selectionId = event.currentTarget.name.substring(
                event.currentTarget.name.lastIndexOf("[") + 1,
                event.currentTarget.name.lastIndexOf("]")
            ),
                preSelectedSection = $('[name*="bundle_option[' + selectionId+ ']"]:checked'),
                selectedSelection = preSelectedSection.length
                    ? preSelectedSection
                    : $('[name*="bundle_option[' + selectionId+ ']"]'),
                selectionValue = selectedSelection.val(),
                previouslyValue = Number($(event.currentTarget).data('prevValue')),
                callback = function (toCompare) {
                    _.forEach(this.specialPricesPool(), function (item) {
                        if (item && item.product_id === toCompare) {
                            this.specialPricesPool.remove(item);
                        }
                    }, this);
                }.bind(this);

            $(event.currentTarget).data('prevValue', $(event.currentTarget).val());

            if (Number(event.currentTarget.value) > 0) {
                callback = function (response) {
                    var canProcess = true;

                    if (!response.length || _.isEmpty(response[0])) {
                        return;
                    }

                    _.forEach(this.specialPricesPool(), function (item) {
                        if (item && item.product_id === response[0]['product_id']) {
                            canProcess = false;

                            return true;
                        }
                    }, this);

                    if (canProcess) {
                        this.specialPricesPool.push(response[0]);
                    }
                }.bind(this);

                /*
                 * Need for handle decrementing value in the case, when value > 0.
                 * It means that if previously the value of input > 0, but decrements,
                 * the program should not send a request to the server.
                 */
                if (previouslyValue < 1) {
                    knockKnockToSpecialPricesApi.getSpecialPrice(selectionValue, callback, this);
                }

                return;
            }

            if (selectedSelection.data('product')) {
                callback(selectedSelection.data('product'));
            }
        }
    };
});