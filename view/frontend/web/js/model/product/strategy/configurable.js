define([
    'underscore',
    'jquery',
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/action/product-special-price'
], function (
    _,
    $,
    knockKnockToSpecialPricesApi
) {
    'use strict';

    return {
        /**
         * Callback for configurable products
         */
        handle: function () {
            var callback = function (response) {
                if (!response.length) {
                    return;
                }

                if (!_.isEmpty(response[0])) {
                    this.specialPricesPool.push(response[0]);
                    this.lastInsertPosition = this.specialPricesPool().length - 1;
                } else {
                    this.lastInsertPosition !== null && this.lastInsertPosition !== undefined
                        ? this.specialPricesPool.remove(this.specialPricesPool()[this.lastInsertPosition])
                        : 0;
                }
            };

            if ($('.swatch-option.selected').length < 2) {
                return;
            }

            knockKnockToSpecialPricesApi.getSpecialPrice(
                $('[data-role="swatch-options"]').data('mageSwatchRenderer').getProduct(),
                callback,
                this
            );
        }
    };
});