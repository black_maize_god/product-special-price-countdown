define([
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/model/product/strategy/configurable',
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/model/product/strategy/grouped',
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/model/product/strategy/bundle',
], function (
    configurableProductStrategy,
    groupedProductStrategy,
    bundleProductStrategy
) {
    'use strict';

    return {
        /**
         * Resolve strategy, which will be used
         * @param {string} type
         * @return {null|Object}
         */
        resolve: function (type) {
            var strategy = null;

            switch (type) {
                case 'configurable':
                    strategy = configurableProductStrategy;
                    break;
                case 'grouped':
                    strategy = groupedProductStrategy;
                    break;
                case 'bundle':
                    strategy = bundleProductStrategy;
                    break;
                default:
                    break;
            }

            return strategy;
        }
    };
});