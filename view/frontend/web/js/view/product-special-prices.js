define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'underscore',
    'jquery',
    'ko',
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/countdown-renderer',
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/model/product/strategy',
    'domReady!'
], function (
    Component,
    customerData,
    _,
    $,
    ko,
    countdownRenderer,
    productTypeStrategy
) {
    'use strict';

    return Component.extend({
        /**
         * Component Configuration
         */
        defaults: {
            isShowAllCounters: 0,
            countdownItemSelector: '.countdown-content-item'
        },

        /**
         * Constructor
         */
        initialize: function () {
            this._super();

            this.isShowAllCounters = ko.observable(Boolean(this.isShowAllCounters));
            this.specialPricesPool = ko.observableArray([]);
            this.specialPricesPool.subscribe(function () {
                var $element = $(this.countdownItemSelector);

                if (!this.isShowAllCounters()) {
                    if ($element.html()) {
                        $element.countdown('remove');
                        $element.html('');
                    }

                    this.setCountDown(
                        document.querySelector(this.countdownItemSelector),
                        this.getNearestToExpire()
                    );
                }
            }, this, "arrayChange");

            this.prepareSpecialPrices();

            // For correctly processing special prices for configurable products
            $('[data-role="swatch-options"]').on(
                'change',
                productTypeStrategy.resolve('configurable').handle.bind(this)
            );

            // For correctly processing special prices for grouped products
            $('[data-selector*="super_group"]').on(
                'input',
                productTypeStrategy.resolve('grouped').handle.bind(this)
            );

            // For correctly processing special prices for bundle products
            $(productTypeStrategy.resolve('bundle').handleAll.bind(this));
            $('[name*="bundle_option"]:radio:not([name*="_qty"])')
                .on('change',  productTypeStrategy.resolve('bundle').handle.bind(this));
            $('[name*="bundle_option_qty"]').on(
                'change input',
                productTypeStrategy.resolve('bundle').handleQtyChanging.bind(this)
            );
        },

        /**
         * Refresh special prices
         */
        specialPricesPoolRefresh: function (element, event) {
            var data = this.specialPricesPool().slice(0);

            if (event.originalEvent === undefined) {
                return;

            }

            this.isShowAllCounters(!this.isShowAllCounters());
            this.specialPricesPool([]);
            this.specialPricesPool(data);
        },

        /**
         * Prepare special prices
         */
        prepareSpecialPrices: function () {
            customerData.reload(['current-product-special-prices']).success(function (response) {
                var specialPrices = response['current-product-special-prices']['product_special_prices'];

                this.specialPricesPool(specialPrices || []);
            }.bind(this));
        },

        /**
         * Connect countdown widget to the element
         */
        setCountDown: countdownRenderer.render.bind(countdownRenderer),

        /**
         * Get first special price, nearest to expire
         * @return {*}
         */
        getNearestToExpire: function () {
            var nearestToExpireItem = {};

            if (!this.specialPricesPool().length) {
                return;
            }

            nearestToExpireItem = this.specialPricesPool()[0];

            _.forEach(this.specialPricesPool(), function (specialPrice) {
                var nearestToExpireTimeStamp = Date.parse(nearestToExpireItem.expires) / 100,
                    specialPriceExpiresTimestamp = Date.parse(specialPrice.expires) / 100;

                if (specialPriceExpiresTimestamp < nearestToExpireTimeStamp) {
                    nearestToExpireItem = specialPrice;
                }
            }, this);

            return nearestToExpireItem;
        }
    });
});