define([
    'PerspectiveStudio_ProductSpecialPriceCountdown/js/action/product-special-price'
], function (
    knockKnockToSpecialPricesApi
) {
    'use strict';

    var component = Object.create(knockKnockToSpecialPricesApi);

    component.apiEndPointRoute = 'rest/V1/special-prices/bundle/';

    return component;
});