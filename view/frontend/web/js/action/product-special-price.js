define([
    'jquery',
    'mage/url'
], function (
    $,
    urlBuilder
) {
    'use strict';

    return {
        apiEndPointRoute: 'rest/V1/special-prices/',

        /**
         * Get special price from the server by entity id.
         * It needs for correctly processing not simple products.
         * @param {int|string} id
         * @param {function} callback
         * @param bindElement
         */
        getSpecialPrice: function (id, callback, bindElement) {
            var path = this.apiEndPointRoute + id;

            $.ajax({
                type: 'get',
                context: this,
                url: urlBuilder.build(path),
                success: callback.bind(bindElement)
            });
        }
    };
});