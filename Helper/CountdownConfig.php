<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Helper;

class CountdownConfig extends \Magento\Framework\App\Helper\AbstractHelper
{
    public const XML_PATH_SHOW_ALL_COUNTERS = 'catalog/price/show_all_special_prices';

    /**
     * @return bool
     */
    public function getIsShowAllCounters(): bool
    {
        return (bool) $this->scopeConfig->getValue(
            static::XML_PATH_SHOW_ALL_COUNTERS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
