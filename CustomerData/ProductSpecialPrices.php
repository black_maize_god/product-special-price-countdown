<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\CustomerData;

class ProductSpecialPrices implements \Magento\Customer\CustomerData\SectionSourceInterface
{
    /**
     * @var \Magento\Catalog\Model\Session $catalogSession
     */
    private $catalogSession;

    /**
     * @var \PerspectiveStudio\ProductSpecialPriceCountdown\Service\SpecialPrices
     */
    private $specialPrices;

    public function __construct(
        \Magento\Catalog\Model\Session $catalogSession,
        \PerspectiveStudio\ProductSpecialPriceCountdown\Service\SpecialPrices $specialPrices
    ) {
        $this->catalogSession = $catalogSession;
        $this->specialPrices = $specialPrices;
    }

    /**
     * @inheritDoc
     */
    public function getSectionData(): array
    {
        $currentProductId = (int) $this->catalogSession->getData('last_viewed_product_id');

        return $this->specialPrices->getAll($currentProductId);
    }
}
