<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\ViewModel;

class Countdown implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var \PerspectiveStudio\ProductSpecialPriceCountdown\Helper\CountdownConfig $countdownConfigHelper
     */
    private $countdownConfigHelper;

    /**
     * Countdown constructor.
     * @param \PerspectiveStudio\ProductSpecialPriceCountdown\Helper\CountdownConfig $countdownConfigHelper
     */
    public function __construct(
        \PerspectiveStudio\ProductSpecialPriceCountdown\Helper\CountdownConfig $countdownConfigHelper
    ) {
        $this->countdownConfigHelper = $countdownConfigHelper;
    }

    /**
     * @return bool
     */
    public function isShowAllCounters(): bool
    {
        return $this->countdownConfigHelper->getIsShowAllCounters();
    }
}
