<?php

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Api;

interface SpecialPricesInterface
{
    /**
     * @param int $productId
     * @route rest/V1/special-prices/{productId}
     * @return array
     */
    public function getPrices(int $productId): array;

    /**
     * @param int $selectionId
     * @route rest/V1/special-prices/bundle/{selectionId}
     * @return array
     */
    public function getPricesForBundle(int $selectionId): array;

    /**
     * @param int $productId
     * @route rest/V1/special-prices/all-by-product/{productId}
     * @return array
     */
    public function getAll(int $productId): array;
}
