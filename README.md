# README #

This module shows products special prices' countdown
below the "Add to Cart" button on the product page.

Module adds a configuration, which allows showing all counters or one,
the nearest to the expiry. Please see:
Stores -> Configuration -> Catalog -> Catalog -> Price -> Show All Special Prices

### Task related to: ###
###### https://perspectivestudio.atlassian.net/browse/STD-219 ######