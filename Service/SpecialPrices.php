<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductSpecialPriceCountdown\Service;

use PerspectiveStudio\ProductSpecialPriceCountdown\Model\CatalogRule\SpecialPriceAggregator as RuleSpecPriceAggregator;
use PerspectiveStudio\ProductSpecialPriceCountdown\Model\Product\SpecialPriceAggregator as ProductSpecPriceAggregator;
use PerspectiveStudio\ProductSpecialPriceCountdown\Model\Product\Bundle\ProductPerSelectionAggregator
    as ProductPerSelectionAggregatorAlias;

class SpecialPrices implements \PerspectiveStudio\ProductSpecialPriceCountdown\Api\SpecialPricesInterface
{
    protected const MERGED_RULES = [
        'simple',
        'virtual',
        'downloadable'
    ];

    /**
     * @var ProductSpecPriceAggregator $productSpecialPriceAggregator
     */
    private $productSpecialPriceAggregator;

    /**
     * @var ProductPerSelectionAggregatorAlias $productPerSelectionAggregator
     */
    private $productPerSelectionAggregator;

    /**
     * @var RuleSpecPriceAggregator $catalogRuleSpecialPriceAggregator
     */
    private $catalogRuleSpecialPriceAggregator;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    private $logger;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    private $productRepository;

    /**
     * SpecialPrices constructor.
     * @param ProductSpecPriceAggregator $productSpecialPriceAggregator
     * @param ProductPerSelectionAggregatorAlias $productPerSelectionAggregator
     * @param RuleSpecPriceAggregator $catalogRuleSpecialPriceAggregator
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        ProductSpecPriceAggregator $productSpecialPriceAggregator,
        ProductPerSelectionAggregatorAlias $productPerSelectionAggregator,
        RuleSpecPriceAggregator $catalogRuleSpecialPriceAggregator,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->productSpecialPriceAggregator = $productSpecialPriceAggregator;
        $this->productPerSelectionAggregator = $productPerSelectionAggregator;
        $this->catalogRuleSpecialPriceAggregator = $catalogRuleSpecialPriceAggregator;
        $this->logger = $logger;
        $this->productRepository = $productRepository;
    }

    /**
     * @inheritDoc
     */
    public function getPrices(int $productId): array
    {
        return ['product_special_prices' => $this->productSpecialPriceAggregator->setProduct($productId)->aggregate()];
    }

    /**
     * @inheritDoc
     */
    public function getPricesForBundle(int $selectionId): array
    {
        $productId = $this->productPerSelectionAggregator->setSelectionId($selectionId)->aggregate();

        return array_merge($this->getPrices($productId), ['selection_id' => $selectionId]);
    }

    /**
     * @inheritDoc
     */
    public function getAll(int $productId): array
    {
        try {
            $currentProduct = $this->productRepository->getById($productId);

            $specialPrices = $this->catalogRuleSpecialPriceAggregator
                ->setProduct($currentProduct)
                ->aggregate();

            if (in_array($currentProduct->getTypeId(), static::MERGED_RULES)) {
                $specialPrices[] = $this->productSpecialPriceAggregator->setProduct($currentProduct)->aggregate();
            }

            return ['product_special_prices' => $specialPrices];
        } catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
            $this->logger->error($exception->getMessage());

            return [];
        }
    }
}
